package com.sda.model;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;

public class FileContentWriter {

	private String path;
	private boolean appendToFile = false;

	public FileContentWriter(String filepath) {
		path = filepath;
	}

	public FileContentWriter(String filepath, boolean appendValue) {
		path = filepath;
		appendToFile = appendValue;

	}
}
